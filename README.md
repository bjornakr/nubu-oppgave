Rapportering om observasjonsdata
================================

Filen `9999-36-XX.t3a` inneholder data om en forelders samhandling med barn. Vi ønsker å se på hvordan et barn reagerer på en beskjed gitt av forelderen, innen en gitt tid. For å kunne si noe om dette må vi se på hva datafilen inneholder:

## Datafilens innhold

```
Task ChildID Age      Observer    FMembers        Date     Time   dbl_int is_cut                                 
 1   9999    36       XX          12              11/23/13 17:38  0       0
  1  -00100    44.0
  1   17222    44.03
  1   11222    45.1
  1   11222    47.43
...
```

* I de to første linjene ligger headeren. Vi kan se bort fra disse.
* Deretter inneholder hver linje en spesifikk observasjon, med følgende innhold:
    * Et oppgavenummer - ikke relevant for denne oppgaven
    * En observasjonskode
    * Et tidspunkt målt i sekunder

## Observasjonskodens innhold
Koden består av fem siffer.

```

+------ Avsender [ mor (3) | far (2) | jente (8) | gutt (1) ]
|+----- Atferdskode, første siffer
||+---- Atferdskode, andre siffer
|||+--- Mottaker [ mor (3) | far (2) | jente (8) | gutt (1) ]
||||+-- Affekt (irrelevant)  
|||||
#####

```

* Å gi en beskjed har atferdskode 42.
* Å følge en beskjed har atferdskode 01.

## Oppgaven

Vi ønsker å se hvor mange ganger i løpet av en oppgave (dvs. en datafil):

1. En forelder gir en beskjed (atferdskode 42).
2. Barnet følger beskjeden (atferdskode 01) innen seks sekunder.

Eksempel:
```
24281    54.6 <-- Far gir en beskjed til barnet (jente).
17122    56.1 <-- Irrelevant observasjon.
80123    58.3 <-- Her følger barnet beskjeden etter 3.7 sekunder.
```

Du må lage et program som leser en fil med observasjonsdata, og rapporterer antall forekomster av sekvensen "gi beskjed-følge beskjed" som vi har beskrevet her.

* **Input:** Navnet på filen med observasjonsdata.
* **Output:** Til standard output, rapporter antall forekomster av sekvensen.

Programmet kan skrives i et valgfritt språk. Det skal legges ved bygge-/kjøreinstruksjoner. Inkluder gjerne automatiserte tester og hvordan man kan kjøre disse. Kodekvalitet vil bli vektlagt. Programmet kan leveres via github/bitbucket eller som en zip-pakke.

### Eksempel på kjøring av et program laget i Java:
```
> java ObsReporter 9999-36-XX.t1
Antall sekvenser: 3
```

